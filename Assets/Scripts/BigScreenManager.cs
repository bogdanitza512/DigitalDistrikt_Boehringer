﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class BigScreenManager : MonoBehaviour
{

	#region Fields and Properties

	[SerializeField]
	Image poster;

	[SerializeField]
	List<Sprite> posterList
	= new List<Sprite>();

	[SerializeField]
	int lastPosterIndex;

    int posterIndex
	{
		get { return lastPosterIndex; }
		set { lastPosterIndex = value % (posterList.Count - 1); }
	}

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
		ShowNextPoster();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

	public void ShowNextPoster()
	{
		poster.sprite = posterList[++posterIndex];
	}

    #endregion
}
