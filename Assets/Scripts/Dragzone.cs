﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class Dragzone : MonoBehaviour
{

    #region Nested Types

    enum DragzoneType { Canvas, TableSocketA, TableSocketB }

    #endregion

    #region Fields and Properties

    public PosterIdentifier posterIdentifier;

    [SerializeField]
	DragzoneType type;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods


	public void UnregisterDraggedObject()
	{
		switch (type)
		{
			case DragzoneType.TableSocketA:
                posterIdentifier.RegisterTableA(PosterIdentifier.TableType.None);
				break;
			case DragzoneType.TableSocketB:
                posterIdentifier.RegisterTableB(PosterIdentifier.TableType.None);
				break;
		}
	}


	public void RegisterDraggedObject(Transform obj)
	{
		obj.position = transform.position;
		var payload = obj.gameObject.GetComponent<DragablePayload>();
		if(payload != null)
		{
			payload.lastDragzone.UnregisterDraggedObject();
			payload.lastDragzone = this;
			switch(type)
			{
				case DragzoneType.TableSocketA:
                    posterIdentifier.RegisterTableA(payload.type);
					break;
				case DragzoneType.TableSocketB:
                    posterIdentifier.RegisterTableB(payload.type);
					break;
			}
		}
			
	}

    #endregion
}
