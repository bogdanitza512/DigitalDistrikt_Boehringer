﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using System;
using System.IO;


/// <summary>
/// 
/// </summary>
public class PlayerConfig : MonoBehaviour
{

	#region Fields and Properties
    
	[ShowInInspector]
	string configFileName = "config.json";

	public string fileName;
    public string cmdArgs;
    public string printerName;
    public List<string> debugOut;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
		LoadConfigData();
    }


	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
    {
       
    }

    #endregion

    #region Methods

	[Button("Load Config Data",ButtonSizes.Medium)]
	public void LoadConfigData()
    {
		var configFilePath = Path.Combine(Application.streamingAssetsPath, configFileName);
		if(File.Exists(configFilePath))
		{
			string dataAsJSON = File.ReadAllText(configFilePath);
			JsonUtility.FromJsonOverwrite(dataAsJSON, this);
			print(dataAsJSON);
			//data = JsonUtility.FromJson<ConfigData>(dataAsJSON);
		}
    }

	[Button("Save Config Data", ButtonSizes.Medium)]
	public void SaveConfigData()
    {
        var configFilePath = Path.Combine(Application.streamingAssetsPath, configFileName);
        if (File.Exists(configFilePath))
        {
			string dataAsJSON = JsonUtility.ToJson(this,true);
			File.WriteAllText(configFilePath,dataAsJSON);
			print(dataAsJSON);
        }
    }

    public void DebugPrint(string text)
    {
        debugOut.Add(text);
        SaveConfigData();
    }

    #endregion
}
