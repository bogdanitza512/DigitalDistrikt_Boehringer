﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniExt;
using Sirenix.OdinInspector;

/// <summary>
/// Player Controller Class
/// </summary>
public class PlayerController : SerializedMonoBehaviour
{

    #region Fields and Properties

    public GameMode gameMode;

    /// <summary>
    /// The type of the current player.
    /// </summary>
    [Tooltip("The type of the current player.")]
    public GameMode.Type gameModeType;

    [SerializeField]
    private KeyCode restartKey = KeyCode.R;

    [SerializeField]
    private KeyCode exitKey = KeyCode.Q;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if(Input.GetKeyDown(restartKey))
        {
            //gameMode.RestartScene();
        }

        if (Input.GetKeyDown(exitKey))
        {
            //gameMode.QuitGame();
        }
    }

    #endregion

    #region Methods

	

    #endregion
}
