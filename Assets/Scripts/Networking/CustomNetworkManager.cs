﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

/// <summary>
/// 
/// </summary>
public class CustomNetworkManager : NetworkManager
{
	#region Nested Types

    //subclass for sending network messages
    public class NetworkMessage : MessageBase
    {
		public GameMode.Type chosenType;
    }


    #endregion


    #region Fields and Properties



    #endregion


    public GameMode gameMode;
 

    #region Unity Messages

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader)
    {
        NetworkMessage message = extraMessageReader.ReadMessage<NetworkMessage>();
        var selectedClass = message.chosenType;
        Debug.Log("server add with message " + selectedClass);

//        GameObject player;
//        Vector3 playerStartPos = GameMode.Instance.specsMap[selectedClass].spawnPos;

//        player = Instantiate(GameMode.Instance.specsMap[selectedClass].playerPrefab
//		                             playerStartPos,
//                             Quaternion.identity) as GameObject;

//        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);

    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        NetworkMessage test = new NetworkMessage
        {
            chosenType = gameMode.currentGameMode
        };

        ClientScene.AddPlayer(conn, 0, test);
    }
   
    #endregion

    #region Methods

	

    #endregion
}
