﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniExt;
using System;
using UnityEngine.Events;

/// <summary>
/// 
/// </summary>
public class PosterIdentifier : MonoBehaviour
{

	#region Nested Types

    [System.Serializable]
    public class PosterIDChanged : UnityEvent<int, TableType, TableType>
    {
    }

    public enum TableType {None = 0, Type_A = 1, Type_B = 2 }


    #endregion

	#region Fields and Properties

    [SerializeField]
    string posterNameTemplate = "Poster_{0}{1}{2}";

	public string posterID = "Poster_{0}{1}{2}";

	[SerializeField]
	int posterIndex;

	[SerializeField]
	TableType tableA_Type = TableType.None;

	[SerializeField]
	TableType tableB_Type = TableType.None;
    
	public List<Sprite> posters;

	// suggested signature: 
	// void OnPosterIDChanged(int posterIndex, PosterIdentifier.TableType tableA_Type, PosterIdentifier.TableType tableB_Type)
    public PosterIDChanged OnPosterIDChanged;

    #endregion

    #region Unity Messages
    
	/*
    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }
    */

    #endregion

    #region Methods

    void ResolveID()
	{
        posterID = posterNameTemplate.FormatWith(posterIndex,
									            (int)tableA_Type,
									            (int)tableB_Type);
        OnPosterIDChanged?.Invoke(posterIndex,
                                  tableA_Type,
                                  tableB_Type);
    }

    public Sprite GetIdentifiedPosterSprite()
	{
		return GetPosterSprite(posterID);
	}

	public Sprite GetPosterSprite(int posterIndex = 0, int tableA_Type = 0, int tableB_Type = 0)
	{
		return GetPosterSprite("Poster_{0}{1}{2}".FormatWith(posterIndex,
															 tableA_Type,
															 tableB_Type));
	}

	public Sprite GetPosterSprite(string id)
    {
		return posters.Find((Sprite obj) => obj.name == id);
    }

	public void RegisterTableA(TableType _type)
	{
		if(tableA_Type != _type)
		{
			tableA_Type = _type;
            ResolveID();
		}
	}

	public void RegisterTableB(TableType _type)
    {
		if (tableB_Type != _type)
        {
            tableB_Type = _type;
            ResolveID();
        }
    }

    public void RegisterPosterIndex(int index)
	{
		posterIndex = index;
		ResolveID();
	}

	public void Reset()
	{
		tableA_Type = TableType.None;
		tableA_Type = TableType.None;
	}

	#endregion
}
