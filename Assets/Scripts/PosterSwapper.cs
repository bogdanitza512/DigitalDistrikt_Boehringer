﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class PosterSwapper : MonoBehaviour
{
    /*

    #region Nested Types

    [System.Serializable]
    struct Thumbnail 
    {
        public Image image;
        public int posterIndex;

        public void Set(Sprite _sprite, int _index)
        {
            image.sprite = _sprite;
            posterIndex = _index;
        }
    }

    [System.Serializable]
    struct TrivialPoster 
    {
        public Sprite sprite;
        public bool isInUse;
        public int index;

        public void Set(bool _inUse)
        {
            isInUse = _inUse;
        }
    }

    #endregion

    #region Fields and Properties

    [OnValueChanged("UpdateThumbnails",true)]
    [SerializeField]
    List<Thumbnail> thumbnailList;

    [SerializeField]
    Image bigPoster;

    [SerializeField]
    List<TrivialPoster> trivialPosters;


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

    public void OnPosterIDChanged(int posterIndex = 0,
                                  PosterIdentifier.TableType tableA_Type = PosterIdentifier.TableType.None,
                                  PosterIdentifier.TableType tableB_Type = PosterIdentifier.TableType.None)
    {
        bigPoster.sprite = PosterIdentifier.Instance.GetIdentifiedPosterSprite();
        print("Called");

        foreach(var thumbnail in thumbnailList)
        {
            if(thumbnail.posterIndex == posterIndex)
            {
                foreach(var poster in trivialPosters)
                {
                    if(poster.isInUse == false)
                    {
                        trivialPosters[thumbnail.posterIndex].Set(false);
                        thumbnail.Set(poster.sprite, poster.index);
                        poster.Set(true);
                    }
                }

            }
        }

    }

    void OnThumbnailButton_Clicked(int thumbnailID)
    {
        
        //thumbnailList[thumbnailID].sprite = PosterIdentifier.Instance.GetIdentifiedPosterSprite(); 
    }

    public void UpdateThumbnails()
    {
        foreach (var thumbnail in thumbnailList)
        {
            trivialPosters[thumbnail.posterIndex].Set(true);
            thumbnail.Set(trivialPosters[thumbnail.posterIndex].sprite,
                          trivialPosters[thumbnail.posterIndex].index);
            trivialPosters[thumbnail.posterIndex].Set(true);
            
        }
    }

    #endregion

    */

    #region Fields and Properties

    public PosterIdentifier posterIdentifier;

    public List<Image> thumbnails;

    public Image bigPoster;

    public List<Sprite> posters;



    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {

    }

    #endregion

    #region Methods

    public void OnThumbnailButton_Clicked(int index)
    {

        var sidePoster = posters.Find((Sprite obj) => obj.Equals(thumbnails[index].sprite));

        var biggiePoster = posters.Find((Sprite obj) => obj.Equals(bigPoster.sprite));

        bigPoster.sprite = sidePoster;
        thumbnails[index].sprite = biggiePoster;

        posterIdentifier.RegisterPosterIndex(posters.FindIndex((obj) => obj.Equals(bigPoster.sprite)));

    }

    public void OnPosterIDChanged(int posterIndex, PosterIdentifier.TableType tableA_Type,PosterIdentifier.TableType tableB_Type)
    {
        print("Holla");
        if (bigPoster.sprite.Equals(posters[posterIndex])) return;


        foreach (var thumnail in thumbnails)
        {
            if (thumnail.sprite.Equals(posters[posterIndex]))
            {
                var sidePoster = posters[posterIndex];

                var biggiePoster = posters.Find((Sprite obj) => obj.Equals(bigPoster.sprite));

                bigPoster.sprite = sidePoster;
                thumnail.sprite = biggiePoster;
            }
        }

        bigPoster.sprite = posters[posterIndex];
        // PosterIdentifier.Instance.RegisterPosterIndex(posters.FindIndex((obj) => obj.Equals(bigPoster.sprite)));
    }

    /*
    public void OnCompleteButton_Clicked()
    {
        printTemplateManager.SetPosterToID(posterIdentifier.posterID);
        canvasGroup.DOFade(0, 2.5f)
                   .OnComplete(() => gameObject.SetActive(false));
    }
*/

    #endregion

}
