﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UniExt;
using UnityEngine.UI.Extensions;

/// <summary>
/// 
/// </summary>
[RequireComponent(typeof(Animator))]
public class ChooseYourTemplateScreen : ScreenMgr.AnimatorScreen
{

    #region Fields and Properties

    public PosterIdentifier posterIdentifier;

    [SerializeField]
    HorizontalScrollSnap posterGalery;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {

	}

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    private void OnDisable()
    {
        //PosterIdentifier.Instance.RegisterPosterIndex(posterGalery.CurrentPage);
    }

    private void OnEnable()
    {
        // PosterIdentifier.Instance.RegisterPosterIndex(posterGalery.CurrentPage);
    }



    #endregion

    #region Methods

    public void OnPageNumber_Changed(int index)
    {
        posterIdentifier.RegisterPosterIndex(index);
    }
    

    public void OnSelectButton_Clicked()
	{      
        posterIdentifier.RegisterPosterIndex(posterGalery.CurrentPage);
        print(posterGalery.CurrentPage);
	}
    
    #endregion

}
