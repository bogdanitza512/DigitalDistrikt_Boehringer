﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.SceneManagement;
using ScreenMgr;

/// <summary>
/// 
/// </summary>
[RequireComponent(typeof(Animator))]
public class EndScreen : AnimatorScreen
{
    [SerializeField]
    float exitTimeDuration = 2.5f;

    #region Fields and Properties

    public GameMode gameMode;

    #endregion

    #region Unity Messages



    #endregion

    #region Methods

    public override void OnAnimationIn()
    {
        base.OnAnimationIn();
        DOTween.Sequence()
               .AppendInterval(exitTimeDuration)
               .OnComplete(() => gameMode.RestartScene());
    }

    #endregion
}
