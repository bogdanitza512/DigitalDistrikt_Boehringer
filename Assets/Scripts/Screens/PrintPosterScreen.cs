﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using System.IO;

/// <summary>
/// 
/// </summary>
[RequireComponent(typeof(Animator))]
public class PrintPosterScreen : ScreenMgr.AnimatorScreen
{

    #region Fields and Properties

    public PosterIdentifier posterIdentifier;
    public PlayerConfig config;

    [SerializeField]
    Image bigPoster;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

    public void OnPosterIDChanged(int posterIndex, PosterIdentifier.TableType tableA_Type, PosterIdentifier.TableType tableB_Type)
    {
        bigPoster.sprite = posterIdentifier.GetIdentifiedPosterSprite();
    }

    public void OnPrintButton_Clicked()
    {
        var pathToPDF = Path.Combine(Application.streamingAssetsPath,
                                     posterIdentifier.posterID + ".pdf");
        var cmdArgs = config.cmdArgs + " "
                            + pathToPDF + " "
                            + config.printerName;
        

        var fileName = config.fileName;
        print(cmdArgs);

        /*
        System.Diagnostics.Process.Start(PlayerConfig.Instance.fileName,
                                         PlayerConfig.Instance.cmdArgs + " "
                                         + pathToPDF + " "
                                         + PlayerConfig.Instance.printerName);

        */

        config.DebugPrint(cmdArgs);

        //#if UNITY_STANDALONE_WIN
        System.Diagnostics.Process.Start(fileName, cmdArgs);
        //#endif
    }

    #endregion
}
