﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using UniExt;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class SubscriptionManager : MonoBehaviour
{
	#region Fields and Properties

	[SerializeField]
	string fileName = "ConsumerData.csv";

	[SerializeField]
    List<string[]> rowData = new List<string[]>();

	[SerializeField]
    string lastName;

	[SerializeField]
    string firstName;

	[SerializeField]
    string speciality;

	[SerializeField]
    string city;

	[SerializeField]
    string email;

	[SerializeField]
	CanvasGroup canvasGroup;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
	{
		//CreateFirstRow();
		Save();
	}

	#endregion

	#region Methods

	VirtualKeyboard vk = new VirtualKeyboard();

    public void OnInputField_Selected()
	{
		vk.ShowTouchKeyboard();
	}

	public void OnInputField_Deselected()
    {
		vk.HideTouchKeyboard();
    }

	private void CreateFirstRow()
    {
        // Creating First row of titles manually..
        string[] rowDataTemp = new string[5];
        rowDataTemp[0] = "Nume";
        rowDataTemp[1] = "Prenume";
        rowDataTemp[2] = "Specializare";
        rowDataTemp[3] = "Oras";
        rowDataTemp[4] = "Email";
        rowData.Add(rowDataTemp);
    }

    private void MakeEntry(string _lastName, string _firstName, string _speciality, string _city, string _email)
    {
        // Creating First row of titles manually..
        string[] rowDataTemp = new string[5];
        rowDataTemp[0] = _lastName;
        rowDataTemp[1] = _firstName;
        rowDataTemp[2] = _speciality;
        rowDataTemp[3] = _city;
        rowDataTemp[4] = _email;
        rowData.Add(rowDataTemp);
    }

	void Save()
    {
        string[][] output = new string[rowData.Count][];

        for (int i = 0; i < output.Length; i++)
        {
            output[i] = rowData[i];
        }

        int length = output.GetLength(0);
        string delimiter = ",";

        StringBuilder sb = new StringBuilder();

        for (int index = 0; index < length; index++)
            sb.AppendLine(string.Join(delimiter, output[index]));


        string filePath = getPath();

		StreamWriter outStream = File.AppendText(filePath);
        outStream.WriteLine(sb);
        outStream.Close();
		rowData.Clear();
    }

	// Following method is used to retrive the relative path as device platform
	private string getPath()
	{
		return Application.streamingAssetsPath + "/" + fileName;
	}

    public void OnEndEdit_LastName(string input)
	{
		lastName = input;
	}
    
	public void OnEndEdit_FirstName(string input)
    {
		firstName = input;
    }
     
	public void OnEndEdit_Speciality(string input)
    {
		speciality = input;
    }

	public void OnEndEdit_City(string input)
    {
		city = input;
    }

	public void OnEndEdit_Email(string input)
    {
		email = input;
    }

	public CanvasGroup textCanvasGorup;
    public void OnSaveButton_Clicked()
	{
		MakeEntry(lastName, firstName, speciality, city, email);
		Save();
		DOTween.Sequence()
		.Append(canvasGroup.DOFade(0, 2.5f))
		.Join(textCanvasGorup.DOFade(1, 2.5f))
		.AppendInterval(5.0f)
		.Append(textCanvasGorup.DOFade(0, 2.5f));
		// canvasGroup.DOFade(0, 2.5f);
	}
    #endregion
}
