using System;

using UnityEngine;
using System.Collections;

namespace UniExt
{
    /// <summary>
    /// Extension methods for Unity.Color
    /// </summary>
    public static class ColorExtensions
    {
        
        public static Color WithRed(this Color color, float red)
        {
            color.r = red;
            return color;
        }
        public static Color WithGreen(this Color color, float green)
        {
            color.g = green;
            return color;
        }
        public static Color WithBlue(this Color color, float blue)
        {
            color.b = blue;
            return color;
        }
        public static Color WithAlpha(this Color color, float alpha) {
            color.a = alpha;
            return color;
        }

        public static Color WithAlpha(this Color original, float? r = null, float? g = null, float? b = null, float? a = null)
        {
            float newR = r.HasValue ? r.Value : original.r;
            float newG = g.HasValue ? g.Value : original.g;
            float newB = b.HasValue ? b.Value : original.b;
            float newA = a.HasValue ? a.Value : original.a;

            return new Color(newR, newG, newB, newA);
        }
    }
}
