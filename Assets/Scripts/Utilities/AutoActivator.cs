﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoActivator : MonoBehaviour {

    [SerializeField]
    List<GameObject> gameObjects =  new List<GameObject>();


	// Use this for initialization
	void Start () {
        foreach (var obj in gameObjects)
        {
            obj.SetActive(true);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
